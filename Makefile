CFILES = main.c
DATA_FILES = hmm.svg
CC = gcc
#CC = clang
EXE = hmmlock
PREFIX = /usr

EMBEDDED_SVG = yes
SVG_PATH = hmm.svg
ZLOCK_PATH = /usr/bin/zlock

CFLAGS += $(shell pkg-config --cflags xext x11 cairo-xlib-xrender librsvg-2.0)
CFLAGS += -Wall -Werror -O2 -DZLOCK_PATH=\"${ZLOCK_PATH}\"
LIBS += $(shell pkg-config --libs xext x11 cairo-xlib-xrender librsvg-2.0)
ifeq ($(EMBEDDED_SVG),yes)
	LDFLAGS += -Wl,--format=binary -Wl,${SVG_PATH}
	ifeq ($(CC), gcc)
		LDFLAGS += -Wl,--format=default
	else
		LDFLAGS += -Wl,--format=elf
	endif
else
	CFLAGS += -DSVG_PATH=\"${SVG_PATH}\"
endif

${EXE}: check_bin clean
	$(CC) $(CFILES) $(CFLAGS) $(LDFLAGS) -o $(EXE) $(LIBS)

install: ${EXE}
	install -m 0775 ${EXE} ${PREFIX}/bin

check_bin:
	@if test ! -x $(ZLOCK_PATH); then \
	      echo "$(ZLOCK_PATH) does not exist or is not an executable"; \
		  exit 1; \
    fi

clean:
	@rm -rf $(EXE)
