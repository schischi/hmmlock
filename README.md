hmmlock - a zlock wrapper
=========================
This program is only "useful" if you are at EPITA and use the program "zlock"
provided by the school.

Requirements
------------
- x11 with the shape extension
- libcairo-xlib
- librsvg
